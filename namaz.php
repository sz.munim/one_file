<?php
	$arr = array(
		'fajr' => '05:15', 
		'zhuhr' => '13:30', 
		'asr' => '16:40', 
		'maghrib' => '18:20', 
		'isha' => '20:00',
		'sunrise' => '06:15',
		'sunset'  => '18:15'
	);
    echo json_encode($arr);
?>