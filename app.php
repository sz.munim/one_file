<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class App extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('app_model');
		 
		 
	}
	
	public function orderOneClick(){
		//$mobile_no = $this->uri->segment(3);
		//$body = file_get_contents('php://input');
		//$postvars = json_decode($body, true);
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{
			
			//$mobile_no = $_POST['m'];
			$mobile_no = $this->input->post('mobile');

			date_default_timezone_set('Asia/Dhaka');
			$order = array(
						'mobile_no' 	=> $mobile_no,
						'time' 	=> date('Y-m-d H:i:s'),
					);	
					
			$ord_id = $this->app_model->insert_one_click_order($order);
			$arr = array(
				'error' => false, 
				'message' => 'We will call you within a 5 minutes.'
			);
			echo json_encode($arr);
		}
		
		else{
			$arr = array(
				'error' => true, 
				'message' => 'Sorry. Something went worng.'
			);
			echo json_encode($arr);
		}
		
		
	}
	
	
	public function getOneClickOrder(){
		
		$order = $this->app_model->get_one_click_order();
		print_r($order);
		$data['details'] = $order;
		$this->load->view('oneClickOrder_view', $data);
		
	}
	
	public function update(){
		$arr = array(
			'version' => 100, 
			'message' => 'Please update Your app.'
		);
		echo json_encode($arr);
	}
	
	
	
}

?>