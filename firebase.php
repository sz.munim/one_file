<?php

class Push {

    // push message title
    private $title;
    private $message;
    private $image;
    // push message payload
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;

    function __construct() {
        
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }

    public function setPayload($data) {
        $this->data = $data;
    }

    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }

    public function getPush() {
        $res = array();
        $res['data']['title'] = $this->title;
        $res['data']['is_background'] = $this->is_background;
        $res['data']['message'] = $this->message;
        $res['data']['image'] = $this->image;
        $res['data']['payload'] = $this->data;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        $res['data']['notiId'] = 1;
        $res['data']['validity'] = '2017-06-06 23:59:00';
        return $res;
    }

}

#API access key from Google API's Console
    //define( 'API_ACCESS_KEY', 'YOUR-SERVER-API-ACCESS-KEY-GOES-HERE' );
	define('FIREBASE_API_KEY', 'AAAA08z7dd0:APA91bFzzgnupijdB4EMjsQE9nS5TSB-oXC70dGBxW0InE2V7nYculXVzEhOCLyXVRO63rKd4lGghrX8yAdnAuDVT-Eukqp9iBvdoZSqfBAg1wndoEvtWxKha-OpjEAc6v6sD_hYHsOV');

    //$registrationIds = $_GET['id'];
#prep the bundle

	$push = new Push();
	$push->setTitle("title");
	$push->setMessage("message message message message");
	$json = $push->getPush();
	
	
	$payload = array();
	$payload['team'] = 'India';
	$payload['score'] = '5.6';
	
	 $push->setIsBackground(FALSE);
        $push->setPayload($payload);
     $json = $push->getPush();
     $msg = array
          (
		'message' 	=> 'Body  Of Notification',
		'title'	=> 'Title Of Notification'
        
          );
	$fields = array
			(
				//'to'		=> $registrationIds,
				'to' => '/topics/' . "global_bhooterbari",
				'data'	=> $json
			);
	
	
	$url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=' . FIREBASE_API_KEY,
            'Content-Type: application/json'
        );
			
			
#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, $url );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		//$result = curl_exec($ch );
		//curl_close( $ch );
		
		$result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

#Echo Result Of FireBase Server
echo $result;